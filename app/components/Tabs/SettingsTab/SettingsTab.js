import React, { Component } from 'react';
import { Button, Text, View, StyleSheet } from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import { List, ListItem } from 'react-native-elements';

const styles = StyleSheet.create({
  pageTitle: {
    fontSize: 34,
    paddingHorizontal: 15,
    paddingVertical: 9,
    fontWeight: 'bold',
    letterSpacing: -0.41,
    marginTop: 24,

  },
  sectionHeader: {
    marginTop: 20,
    paddingHorizontal: 15,
    paddingVertical: 9,
    color: '#8E8E93',
    fontSize: 13,
    fontFamily: 'System',
  },
  listItemStyle: {
    color: '#000000',
    fontSize: 17,
    fontFamily: 'System',
  }

});


class DetailsScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Details!</Text>
      </View>
    );
  }
}

class SettingsScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'stretch' }}>
        <Text style={styles.pageTitle}>Settings</Text>

        <Text style={styles.sectionHeader}>SECURITY</Text>
        <List containerStyle={{marginBottom: 20, marginTop: 0}}>

          <ListItem
            title={'Backup Wallet'}
            titleStyle={styles.listItemStyle}
          />
          <ListItem
            title={'Display Private Key'}
            titleStyle={styles.listItemStyle}
          />
        </List>

        <Text style={styles.sectionHeader}>ABOUT</Text>
        <List containerStyle={{marginBottom: 20, marginTop: 0}}>

          <ListItem
            title={'Help'}
            titleStyle={styles.listItemStyle}
          />
          <ListItem
            title={'About Us'}
            titleStyle={styles.listItemStyle}
          />
          <ListItem
            title={'Terms of Service'}
            titleStyle={styles.listItemStyle}
          />
          <ListItem
            title={'Privacy Policy'}
            titleStyle={styles.listItemStyle}
          />
        </List>

      </View>
    );
  }
}

export default SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen,
    Details: DetailsScreen,
    //Security
    BackupWallet: DetailsScreen,
    DisplayPrivateKey: DetailsScreen,
    // SetupPIN: ,
    //AboutSection
    Help: DetailsScreen,
    // AboutUs:
    // TermsOfService:
    // PrivacyPolicy:
  },
  {
    navigationOptions: {
      title: 'Settings',
    }
  }
);
