import React, { Component } from 'react';
import { Button, Text, View, ScrollView, Image, StyleSheet } from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import moment from 'moment';

const styles = StyleSheet.create({
  sectionHeader: {
    marginTop: 20,
    paddingHorizontal: 18,
    paddingVertical: 9,
    color: '#333',
    fontSize: 13,
    fontFamily: 'System',
  },
  txnName: {
    fontSize: 18
  },
  txnTime: {
    fontSize: 12,
    paddingVertical: 4,
    color: '#8E8E93'
  },
  txnInfo: {
    fontSize: 12
  },
  txnAmount: {
    fontSize: 18,
    paddingVertical: 4,
  },
  movUnitText: {
    fontSize: 12
  }

});

class DetailsScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Details!</Text>
      </View>
    );
  }
}

class HomeScreen extends Component {
  render() {
    let transactions = [
      {
        txnName: 'Moving Reward',
        txnTime: `${moment().add('days', -1).format('D MMM')} at 9:05 AM`,
        txnInfo: 'Total Distance: 56km',
        txnAmount: '+ 12.13'
      },
      {
        txnName: 'Redemption',
        txnTime: `${moment().add('days', -2).format('D MMM')} at 3:21 PM`,
        txnInfo: 'Starbucks Coupon',
        txnAmount: '- 20.00'
      },
      {
        txnName: 'P2P Transfer',
        txnTime: `${moment().add('days', -2).format('D MMM')} at 8:22 PM`,
        txnInfo: 'Received from *****j2ekx9',
        txnAmount: '+ 34.23'
      },
      {
        txnName: 'Moving Reward',
        txnTime: `${moment().add('days', -2).format('D MMM')} at 9:28 AM`,
        txnInfo: 'Total Distance: 22km',
        txnAmount: '+ 4.89'
      },
      {
        txnName: 'Moving Reward',
        txnTime: `${moment().add('days', -3).format('D MMM')} at 10:05 PM`,
        txnInfo: 'Total Distance: 56km',
        txnAmount: '+ 12.13'
      },
      {
        txnName: 'Redemption',
        txnTime: `${moment().add('days', -4).format('D MMM')} at 3:21 PM`,
        txnInfo: 'Shell Gas Coupon',
        txnAmount: '- 20.00'
      },
      {
        txnName: 'P2P Transfer',
        txnTime: `${moment().add('days', -4).format('D MMM')} at 8:22 PM`,
        txnInfo: 'Received from *****hk3cs82',
        txnAmount: '+ 3.50'
      },
      {
        txnName: 'Moving Reward',
        txnTime: `${moment().add('days', -5).format('D MMM')} at 9:14 AM`,
        txnInfo: 'Total Distance: 22km',
        txnAmount: '+ 8.86'
      },
    ]

    return (
      <ScrollView
        // bounces={false}
        contentContainerStyle={{
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: '#fff',
        paddingBottom: 48
      }}>
        <View
          style={{
            marginTop: 24,
            position: 'relative',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: 220,
          }}>
          <Image
            style={{
              // flex: 1,

              width: '100%',
              height: 220,
              position: 'absolute',
              top: 0,
            }}
            resizeMode="contain"
            source={require('../../../resources/img/wallet/card-design.png')}
          >
          </Image>

          <View
            style={{
              position: 'absolute',
              top: 56,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#ffffff'
              }}
            >
              Balance
            </Text>
            <Text
              style={{
                color: '#ffffff',
                fontSize: 24,
                marginVertical: 8,
              }}
            >
              128.88
            </Text>

            <Text
              style={{
                color: '#ffffff'
              }}
            >
              MOV Tokens
            </Text>

          </View>

          <View
            style={{
              position: 'absolute',
              top: 156,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}>
            <View>
              <Text
                style={{
                  color: '#ffffff',
                  fontSize: 11
                }}
              >
                Wallet Address:
              </Text>
              <Text
                style={{
                  color: '#ffffff',
                  fontSize: 11
                }}
              >
                0xe5dbd1540ee8419b0108632f28894535ec7207eb
              </Text>

            </View>

            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                paddingLeft: 12
              }}
            >
              <SimpleLineIcons name='refresh' color="white" size={25} style={{paddingTop: 4}}/>
              {/* <Text>Refresh</Text> */}
            </View>
          </View>

        </View>

        {/* <View>
          <Text>Recent Transaction</Text>
          <TxnListItem/>
        </View> */}
        <Text
          style={styles.sectionHeader}>
          Transaction Records
        </Text>
          {/* <MonthSwitch />
          <TxnList /> */}

        <View
          style={{
            paddingHorizontal: 18
          }}>
          {
            transactions.map((txn, index)=>{
              return <TransactionRowItem key={index} txn={txn}/>
            })
          }



        </View>



      </ScrollView>
    );
  }
}

export class TransactionRowItem extends Component {
  render(){
    let txn = this.props.txn

    return (
      <View
        style={{
          paddingVertical: 8
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingTop: 8,
            paddingBottom: 14,
            borderBottomWidth: 1,
            borderBottomColor: '#8E8E93',

          }}>
          <View>
            <Text style={styles.txnName}>{txn.txnName}</Text>
            <Text style={styles.txnTime}>{txn.txnTime}</Text>
          </View>
          <View
            style={{
              alignItems: 'flex-end'
            }}>
            <Text style={styles.txnInfo}>{txn.txnInfo}</Text>
            <Text style={styles.txnAmount}>{txn.txnAmount}</Text>
            <Text style={styles.movUnitText}>MOV Tokens</Text>
          </View>
        </View>
      </View>

    )
  }
}


export default WalletStack = createStackNavigator(
  {
    Wallet: HomeScreen,
    Details: DetailsScreen,
  },
  {
    navigationOptions: {
      title: 'Wallet',
    }
  }
);
