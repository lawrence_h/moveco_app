import React, { Component } from 'react';
import { Button, Text, View, ScrollView, Image, StyleSheet, TouchableHighlight } from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { Accelerometer, Gyroscope } from "react-native-sensors";


const styles = StyleSheet.create({
  pageTitle: {
    fontSize: 34,
    paddingHorizontal: 15,
    paddingVertical: 9,
    fontWeight: 'bold',
    letterSpacing: -0.41,
    marginTop: 24,
  },
  statContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginTop: 24,
    marginBottom: 24,
  },
  statTitle: {
    fontSize: 18,
  },
  statNumber: {
    fontSize: 30,
  },
  statUnit: {
    fontSize: 12,
    paddingBottom: 4,
    paddingHorizontal: 4
  },
  statNumContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  carImgContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  amountContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 12
  },
  movAmount: {
    fontSize: 40,
  },
  movMountUnit: {
    fontSize: 12,
  },
  buttonBorderContainer: {
    width: 84,
    height: 84,
    borderWidth: 1,
    borderRadius: 43,
    borderColor: '#333',
    padding: 1,
  },
  redBorder: {
    borderColor: 'tomato',
  },
  pauseButton: {
    width: 80,
    height: 80,
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333',

  },
  endTripButton: {
    width: 80,
    height: 80,
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'tomato'
  },
  buttonText: {
    // color: '#000'
    color: '#fff'
  },
  buttonsContainer: {
    paddingVertical: 24,
    paddingHorizontal: '20%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  }

});

const Value = ({ name, value }) => (
  <View style={styles.valueContainer}>
    <Text style={styles.valueName}>{name}:</Text>
    <Text style={styles.valueValue}>{new String(value).substr(0, 8)}</Text>
  </View>
);


export class MovingScreen extends Component {
  constructor(props) {
    super(props);


    // new Accelerometer({
    //   updateInterval: 1000 // defaults to 100ms
    // })
    //   .then(observable => {
    //     observable.subscribe(({ x, y, z }) => {
    //       this.setState({ ax: x, ay: y, az: z })
    //     });
    //   })
    //   .catch(error => {
    //     console.log("Accelerometer is not available");
    //   });
    //
    // new Gyroscope({
    //   updateInterval: 1000
    // })
    //   .then(observable => {
    //     observable.subscribe(({ x, y, z }) => {
    //       this.setState(state => ({
    //         gx: x,
    //         gy: y,
    //         gz: z
    //         // gy: y + state.y
    //       }));
    //     });
    //   })
    //   .catch(error => {
    //     console.log("gyro is not available");
    //   });


    this.state = {
      ax: 0, ay: 0, az: 0,
      gx: 0, gy: 0, gz: 0,
    };
  }

  render(){
    return (
      <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'stretch', backgroundColor: '#fff' }}>
        <Text style={styles.pageTitle}>Moving</Text>
        <View style={styles.statContainer}>
          <View>
            <Text style={styles.statTitle}>Start Time</Text>
            <View style={styles.statNumContainer}>
              <Text style={styles.statNumber}>11:30</Text>
              <Text style={styles.statUnit}>AM</Text>
            </View>
          </View>

          <View>
            <Text style={styles.statTitle}>Duration</Text>
            <View style={styles.statNumContainer}>
              <Text style={styles.statNumber}>45</Text>
              <Text style={styles.statUnit}>mins</Text>
            </View>
          </View>

          <View>
            <Text style={styles.statTitle}>Distance</Text>
            <View style={styles.statNumContainer}>
              <Text style={styles.statNumber}>10</Text>
              <Text style={styles.statUnit}>km</Text>
            </View>
          </View>

        </View>

        <View style={styles.carImgContainer}>
          <Image
            style={{
              // flex: 1,
              width: '50%',
              height: 60,
            }}
            resizeMode="contain"
            source={require('../../../resources/img/moving/moving-cars.png')}
          >
          </Image>
        </View>

        <View style={styles.amountContainer}>
          <Text style={styles.movAmount}>0.88</Text>
          <Text style={styles.movMountUnit}>MOV Tokens</Text>
        </View>

        <View style={styles.buttonsContainer}>
          <View style={styles.buttonBorderContainer}>
            <TouchableHighlight
              style={styles.pauseButton}
              underlayColor={'#d7d7d7'}
              onPress={()=>{
                // navigation.navigate('Settings')
              }}>
                <Text
                  style={styles.buttonText}
                >
                  Pause
                </Text>
            </TouchableHighlight>
          </View>

          <View style={[styles.buttonBorderContainer, styles.redBorder]}>
            <TouchableHighlight
              style={styles.endTripButton}
              underlayColor={'#d7d7d7'}
              onPress={()=>{
                // navigation.navigate('Settings')
              }}>
                <Text
                  style={styles.buttonText}
                >
                  End Trip
                </Text>
            </TouchableHighlight>
          </View>

        </View>

        {/* <Text
          style={styles.sectionHeader}>
          Past Records
        </Text> */}

        {/* <View style={styles.container}>
          <Text style={styles.headline}>Accelerometer values</Text>
          <Value name="x" value={this.state.ax} />
          <Value name="y" value={this.state.ay} />
          <Value name="z" value={this.state.az} />
        </View>

        <View style={styles.container}>
          <Text style={styles.headline}>Gyroscope values</Text>
          <Value name="x" value={this.state.gx} />
          <Value name="y" value={this.state.gy} />
          <Value name="z" value={this.state.gz} />
        </View> */}

        <View>
          {/* <TransactionRowItem key={index} txn={txn}/> */}
        </View>
      </View>
    )
  }
}


export default MovingStack = createStackNavigator(
  {
    Moving: MovingScreen
  },
  {
    navigationOptions: {
      title: 'Moveco',
    }
  }
);
