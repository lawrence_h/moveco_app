import React, { Component } from 'react';
import { Button, Text, View, ScrollView, Image, Dimensions } from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

class DetailsScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Details!</Text>
      </View>
    );
  }
}

export class RedemptionTile extends Component {
  render(){

    let width = Dimensions.get('window').width; //full width

    return (
      <View
        style={{
          width: (width - 60)/2,

        }}>
        <View
          style={{

          }}>
          <Image
            style={{
              borderRadius: 8,
              marginTop: 24,
              width: (width - 60)/2,
              height: (width - 60)/2,
            }}
            resizeMode="cover"
            source={{uri: `https://s3-ap-southeast-1.amazonaws.com/moveco-app-assets/imgs/${this.props.item.img}`}}
          >
          </Image>
        </View>
        <View
          style={{
            width: (width - 60)/2,
          }}>
          <Text
            style={{
              paddingTop: 4,
              paddingBottom: 2,
              fontSize: 12
            }}>
            {this.props.item.itemName}
          </Text>
          <Text
            style={{
              // paddingVertical: 4,
              fontSize: 14,
              color: 'tomato',
              fontWeight: 'bold'
            }}>
              {this.props.item.movAmount} MOV
          </Text>
        </View>
      </View>
    )

  }
}

class HomeScreen extends Component {
  render() {

    let items = [
      {
        img: 'starbucks.png',
        itemName: 'Starbucks HK$ 50 Coupon',
        movAmount: 10
      },
      {
        img: 'shell.jpg',
        itemName: 'Shell HK$ 100 Coupon',
        movAmount: 20
      },
      {
        img: 'carwash.jpg',
        itemName: 'Carwash HK$ 20 Coupon',
        movAmount: 10
      },
      {
        img: 'flight.jpg',
        itemName: 'Air Miles - 100 Miles',
        movAmount: 100
      },
      {
        img: 'starbucks.png',
        itemName: 'Starbucks HK$ 50 Coupon',
        movAmount: 10
      },
      {
        img: 'shell.jpg',
        itemName: 'Shell HK$ 100 Coupon',
        movAmount: 20
      },
      {
        img: 'carwash.jpg',
        itemName: 'Carwash HK$ 20 Coupon',
        movAmount: 10
      },
      {
        img: 'flight.jpg',
        itemName: 'Air Miles - 100 Miles',
        movAmount: 100
      },
    ]

    return (
      <ScrollView contentContainerStyle={{
        // flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: '#fff',
        paddingBottom: 48
        // flexGrow: 1
        // postiion: 'relative'
      }}>
        <Text
          style={{
            color: '#000',
            fontSize: 34,
            fontWeight: 'bold',
            paddingVertical: 9,
            paddingHorizontal: 15,
            marginTop: 24,
          }}
        >
            Redemption
        </Text>
        <View
          style={{
            // marginTop: 100,
            position: 'relative',
            // flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: 220,
            paddingHorizontal: 15,
          }}>
          <Image
            style={{
              flex: 1,
              // marginTop: 12,
              width: '100%',
              height: 220,
              position: 'absolute',
              top: 0,
            }}
            resizeMode="contain"
            source={require('../../../resources/img/wallet/card-design.png')}
          >
          </Image>

          <View
            style={{
              position: 'absolute',
              top: 56,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#ffffff'
              }}
            >
              Balance
            </Text>
            <Text
              style={{
                color: '#ffffff',
                fontSize: 24,
                marginVertical: 8,
              }}
            >
              128.88
            </Text>

            <Text
              style={{
                color: '#ffffff'
              }}
            >
              MOV Tokens
            </Text>

          </View>

          <View
            style={{
              position: 'absolute',
              top: 156,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}>
            <View
              style={{

              }}
            >
              <Text
                style={{
                  color: '#ffffff',
                  fontSize: 11
                }}
              >
                Wallet Address:
              </Text>
              <Text
                style={{
                  color: '#ffffff',
                  fontSize: 11
                }}
              >
                0xe5dbd1540ee8419b0108632f28894535ec7207eb
              </Text>

            </View>

            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                paddingLeft: 12
              }}
            >
              <SimpleLineIcons name='refresh' color="white" size={25} style={{paddingTop: 4}}/>
              {/* <Text>Refresh</Text> */}
            </View>
          </View>

        </View>

        <View
          style={{
            paddingHorizontal: 18,
            flexDirection: 'row',
            justifyContent: 'space-between',
            flexWrap: 'wrap'
          }}
        >
          {/* Redemption Tiles */}
          {
            items.map((item, index) => {
              return <RedemptionTile item={item} key={index}/>
            })
          }

        </View>

      </ScrollView>
    );
  }
}




export default WalletStack = createStackNavigator(
  {
    Wallet: HomeScreen,
    Details: DetailsScreen,
  },
  {
    navigationOptions: {
      title: 'Redemption',
    }
  }
);
