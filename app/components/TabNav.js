import React, { Component } from 'react';
import { Button, Text, View } from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import SettingsTab from './Tabs/SettingsTab/SettingsTab';
import WalletTab from './Tabs/WalletTab/WalletTab';
import MovingTab from './Tabs/MovingTab/MovingTab';
import RedemptionTab from './Tabs/RedemptionTab/RedemptionTab';


export default createBottomTabNavigator(
  {
    Wallet: WalletTab,
    Moving: MovingTab,
    Redemption: RedemptionTab,
    Settings: SettingsTab,
  },
  {
    // initialRouteName: 'Moving',
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;

        switch (routeName) {
          case 'Wallet':
            iconName = 'wallet'
            break;
          case 'Moving':
            iconName = 'speedometer'
            break;
          case 'Redemption':
            iconName = 'present'
            break;
          case 'Settings':
            iconName = 'settings'
            break;
          default:

        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <SimpleLineIcons name={iconName} size={25} color={tintColor} style={{paddingTop: 4}}/>;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  }
);
