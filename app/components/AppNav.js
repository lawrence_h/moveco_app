import React, { Component } from 'react';
import { SafeAreaView, View, Text } from 'react-native';
import TabNav from './TabNav'

export default class AppNav extends Component {
  constructor(props){
    super(props);

  }

  render(){
    return (
      // <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={{flex: 1}}>
          <TabNav />
        </View>
      // </SafeAreaView>
    )
  }
}
